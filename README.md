# CONTADORES_AGUA

## Descripción

CONTADORES_AGUA es un sistema de big data diseñado para el procesamiento de datos en tiempo real que involucra la generación de contadores y pulsos de datos a intervalos regulares. Estos datos se envían a través de un productor a un clúster de Apache Kafka, donde son consumidos por un consumidor que los ingresa en una base de datos Cassandra. Tanto Kafka como Cassandra se ejecutan en contenedores Docker.

## Características principales

- Generación de contadores y pulsos, tratamiento de datos en tiempo real.
- Integración con Apache Kafka para la transmisión y el procesamiento eficiente de datos.
- Almacenamiento y consulta de datos en una base de datos NoSQL Cassandra.
- Utilización de una API Flask para la interacción entre el consumidor y Cassandra.
- Aprovisionamiento y ejecución en contenedores Docker para una gestión sencilla y escalabilidad.
- Alta concurrencia y uso de varios productores y consumidores para optimizar el proceso de generación, envío y recepción de datos.
- El consumidor imprime en un txt los outliers y muestra los valores normales por pantalla, todos los datos son enviados a Cassandra.

## Estructura

Productor: Desarrollado en Java, proyecto llamado "ProductorJava_ca"
Consumidor: Desarrollado en Python, proyecto llamado "ConsumidorPython_ca"
API: Api Flask desarrollada en Python, proyecto llamado "ApiFlaskCassandra_ca"
Kafka: Alojado en un contenedor de Docker, llamado "kafka_server_ca"
Cassandra: Base de datos Cassandra alojada en un contenedor de Docker con el nombre de "cassandra_ca"

## Puesta en marcha

Para que el sistema funcione correctamente...
- Es necesario abrir un puerto en la máquina local y redirigir su salida al puerto por defecto que usa kafka, además de crear los topic "incidencias" y "registros" en el kafka
- Se debe añadir las ip y los puertos en los archivos "data.txt", ya que fueron añadidos al .gitignore por motivos de seguridad
- Hay que crear la keyspace y la tabla contadores en Cassandra, el esquema está en la misma carpeta llamada "Cassandra_ca"

1. Lanzar ambos contenedores de Docker, tanto el kafka como el Cassandra
2. Iniciar la API Flask
3. Ejecutar el Consumidor y posteriormente el Productor

Notas:
- Se han añadido varios intérpretes de Python, uno para la API y otro para el Consumidor con el fin de simplicar el proceso y disponer de las librerías justas y necesarias para la ejecución de este proyecto.
- Además, también se han subido las pruebas realizadas con el cliente rest, en este caso Insomnia, en formato json
- Tener en cuenta que por defecto el productor instancia 100.000 contadores y genera 8 pulsos de datos para cada uno en un intervalo de tiempo de 15 segundos. Aunque se ha optimizado el proceso de forma concurrente puede tardar varios minutos o más en ejecutarse, sobre todo el proceso de enviar los datos a Cassandra es más lento que el de generación de datos, pues implica mostrarlos por pantalla a una velocidad en la que sea legible.

Para realizar pruebas o comprobar su funcionamiento se aconseja modificar las variables estáticas de la clase Main del proyecto "ProductorJava_ca" cambiando sus valores por números cuyo valor sea bajo.

## Objetivos

Los objetivos de este proyecto son:

- Proporcionar una solución eficiente para el procesamiento de datos a tiempo real en un entorno de big data.
- Facilitar la generación, transmisión y almacenamiento de datos de contadores de agua.
- Ofrecer una arquitectura modular y escalable que pueda adaptarse a diversas necesidades y volúmenes de datos.

## Licencia

Este proyecto está protegido por derechos de autor. Todos los derechos están reservados. 
No se concede permiso para copiar, modificar o distribuir el código sin el consentimiento explícito del autor.

Si deseas utilizar, modificar o contribuir a este proyecto, por favor, ponte en contacto con el autor para obtener su autorización.

## Contribuciones

No se admiten contribuciones en este proyecto.

## Autor
José Sánchez García
